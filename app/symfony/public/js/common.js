/**
 * Obtain JSON from API and modify the html content. Or just AJAX kind of things.
 * @param $from_api -- role or user api
 * @param $name -- the name of the data
 * @param $el -- the id of the element to modify
 * @param $key -- the key of the json return
 */
function getJson($from_api, $name, $el, $key) {
    // return if the element has already been updated (this whole function is not a great hack!)
    if (getJsonTracker[$el] == null) {
        getJsonTracker[$el] = true;
    } else {
        return
    }

    new Vue({
        el: '#' + $el,
        data: {
            info: ''
        },
        created: function () {
            this.getJson();
        },
        methods: {
            getJson: function () {
                var vm = this;
                // the route for the api
                axios.get('/api/rbac/' + $from_api + '/' + $name)
                    .then(function (response) {
                        // Only the data under the key 'permissions' is needed
                        vm.info = response.data[$key];
                    })
                    .catch(function (error) {
                        vm.info = 'An error occured.' + error;
                    });
            },
        }
    });
}
// Stupid variable used to make sure getJson() is only called once per element
let getJsonTracker = {};

/**
 * Toggles the visibility of a given element
 * @param elementID
 */
function toggleVisibility(elementID) {
    var pageElement = document.getElementById(elementID);
    var displayMode = pageElement.style['display'];
    if (displayMode === 'none' || displayMode === '') {
        pageElement.style['display'] = 'block';
    } else if (displayMode === 'block') {
        pageElement.style['display'] = 'none';
    }
}

/**
 * Toggles the visibility of a given element, while hiding all elements with a specific class
 * @param elementID
 * @param hideElementsWithClass
 */
function toggleVisibilityAndHideClassElements(elementID, hideElementsWithClass) {
    var elementsToHide = document.getElementsByClassName(hideElementsWithClass);
    for (var i = 0; i < elementsToHide.length; i++) {
        if (elementsToHide[i].id !== elementID) {
            elementsToHide[i].style['display'] = 'none';
        }
    }
    toggleVisibility(elementID);
}
