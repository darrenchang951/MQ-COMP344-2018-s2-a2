<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Ecom\Rbac\Rbac;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Ecom\Sessionmgmt\Session;

class RbacRolesUpdateController extends AbstractController
{
    /**
     * @Route("/rbac/roles/update", name="rbac_roles_update", methods={"POST"})
     * @param Rbac $rbac
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Rbac $rbac, Request $request)
    {
        // Check if the client is a logged in shopper
        $sessionmgmt = new Session();
        $shopperId = $sessionmgmt->store_get_shopper_id();
        $shopperName = $sessionmgmt->store_get_shopper_name();

        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);
        if (!$rbac->hasPermission($uri)) {
            return $this->render('accessDenied.html.twig', [
                'shopperId' => $shopperId,
                'shopperName' => $shopperName,
            ]);
        }

        // Get client submitted form information
        $action = $request->request->get('action');
        $roleName = $request->request->get('role-name');
        $roleDesc = $request->request->get('role-desc');

        // Redirect back to the form page if the role name is not allowed
        $re = '/[\d\w- ]*/m';
        preg_match($re, $roleName, $match);
        if ($match == null or $match[0] !== $roleName) {
            return $this->redirectToRoute('rbac_roles');
        }

        // Create an array of permissions to be assigned for the role
        $re = '/(?:permission-)(\d*)/m';
        $formData = $request->request->getIterator();
        $rolePerm = [];
        foreach ($formData as $key => $value) {
            preg_match($re, $key, $match);
            if (count($match) > 0) {
                $rolePerm[] = $match[1];
            }
        }

        // add new user
        if ($action === 'add') {
            $rbac->addRole($roleName, $roleDesc);
            $rbac->addPermissionsToRole($roleName, $rolePerm);
        } elseif ($action === 'delete') { // delete role
            if (!$rbac->deleteRole($roleName)) {
                return $this->render('illegalOperation.html.twig', [
                    'shopperId' => $shopperId,
                    'shopperName' => $shopperName,
                    'message' => 'You cannot remove this role because this is the last role with all the permissions, 
                    or the role does not exist.',
                    'prevUri' => $this->generateUrl('rbac_roles'),
                ]);
            };
        } elseif ($action === 'update') { // update role's permissions and description
            $rbac->deletePermissionsFromRole($roleName);
            $rbac->addPermissionsToRole($roleName, $rolePerm);
            $rbac->updateRoleDesc($roleName, $roleDesc);
        }

        return $this->redirectToRoute('rbac_roles');
    }
}
