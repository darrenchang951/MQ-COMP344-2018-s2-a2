<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Ecom\Rbac\Rbac;

class ApiRbacRoleController extends AbstractController
{
    /**
     * @Route("/api/rbac/role/{role_name}", name="api_rbac_role")
     * @param Request $request
     * @param Rbac $rbac
     * @param $role_name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, Rbac $rbac, $role_name)
    {
        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);
        // remove role_name from the url and the leading /
        $uri = str_replace("%20", " ", $uri);
        $uri = str_replace("/" . $role_name, "", $uri);

        // Create Object
        $response = new JsonResponse();

        if ($rbac->hasPermission($uri)) {
            $permissions = $rbac->getRolePermissions($role_name);
            $roleDesc = $rbac->getRoleInfo($rbac->getRoleId($role_name));
            $response->setData(array(
                'permissions' => $permissions,
                'role_desc' => $roleDesc['desc'],
                'role' => $role_name,
            ));
            return $response;
        } else {
            $response->setData(array(
                'permissions' => "",
                'role' => $role_name,
            ));
            return $response;
        }
    }
}
