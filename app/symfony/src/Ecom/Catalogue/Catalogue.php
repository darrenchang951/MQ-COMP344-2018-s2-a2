<?php

namespace App\Ecom\Catalogue;

use App\Ecom\Dbpdo\CommonDb;
use Doctrine\DBAL\Driver\PDOConnection;

/**
 * [ NOTE ]
 * This isn't our subsystem, and, as such, the code contained within this file is whatever.
 * Needed *something* to test on. Whatever...
 * Class Catalogue
 * @package App\Ecom\Catalogue
 */
class Catalogue
{
    private $db;

    public function __construct()
    {
        // Create a database connection for the class
        $this->db = new CommonDb();
        $this->db = $this->db->conn();
    }

    function getCatalogue()
    {
        $stmt = $this->db->prepare('SELECT * FROM Product, ProdPrices WHERE ProdPrices.PrPr_Prod_id = Product.prod_id');
        $stmt->execute();
        $catalogue = $stmt->fetchAll(PDOConnection::FETCH_ASSOC);
        $stmt->closeCursor();

        return $catalogue;
    }

    function getAttributes($product_id)
    {
        $stmt = $this->db->prepare('SELECT * FROM Attribute WHERE Product_prod_id = :PID ORDER BY `name`');
        $stmt->bindParam(':PID', $product_id);
        $stmt->execute();
        $attrs = $stmt->fetchAll(PDOConnection::FETCH_ASSOC);
        $stmt->closeCursor();

        return $attrs;
    }

    function getAttributeValues($product_id, $attr_id)
    {
        $stmt = $this->db->prepare('SELECT * FROM AttributeValue WHERE AttrVal_Prod_id = :PID AND AttrVal_Attr_id = :AID');
        $stmt->bindParam(':PID', $product_id);
        $stmt->bindParam(':AID', $attr_id);
        $stmt->execute();
        $vals = $stmt->fetchAll(PDOConnection::FETCH_ASSOC);
        $stmt->closeCursor();

        return $vals;
    }

    function formatPrice($price)
    {
        if ($price == 0) return '';
        return ($price < 0) ? '(-$' . substr($price, 1) . ')' : '(+$' . $price . ')';
    }
}