<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Ecom\Sessionmgmt\Session;
use App\Ecom\Rbac\Rbac;

class HomePageController extends AbstractController
{
    /**
     * @param Session $session
     * @param Rbac $rbac
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Session $session, Rbac $rbac)
    {
        // Check if the client is a logged in shopper
        $shopperId = $session->store_get_shopper_id();
        $shopperName = $session->store_get_shopper_name();

        // Check what permissions the user has
        $rbacAccess = $rbac->hasPermission('/rbac');

        return $this->render('home_page/index.html.twig', [
            'title' => 'COMP344 E-commerce Technology',
            'rbac' => '/rbac',
            'sessionMgmt' => '/sessionmgmt/login',
            'catalogue' => '/catalogue',
            'shopperId' => $shopperId,
            'shopperName' => $shopperName,
            'rbacAccess' => $rbacAccess,
        ]);
    }
}
