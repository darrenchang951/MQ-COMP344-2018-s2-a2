<?php

namespace App\Ecom\Dbpdo;

use Doctrine\DBAL\Driver\PDOConnection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Dotenv\Dotenv;

/**
 * Class CommonDb
 * This class is used to obtain a PDO connection to the database
 * @package App\Ecom\Dbpdo
 */
class CommonDb extends AbstractController
{
    public function conn()
    {
        // Initialise sensitive variable from .env file
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__.'/.env');
        $dbHost = getenv('DB_HOST');
        $dbName = getenv('DB_Name');
        $dbUser = getenv('DB_USER');
        $dbPass = getenv('DB_PASS');

        $db = new PDOConnection(
            "mysql:host=$dbHost;dbname=$dbName;charset=utf8",
            $dbUser,
            $dbPass);
        return $db;
    }
}
