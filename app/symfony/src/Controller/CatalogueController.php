<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Ecom\Catalogue\Catalogue;
use App\Ecom\Sessionmgmt\Session;

class CatalogueController extends AbstractController
{
    /**
     * @Route("/catalogue", name="catalogue")
     * @param Catalogue $catalogue
     * @param Session $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Catalogue $catalogue, Session $session)
    {
        // Check if the client is a logged in shopper
        $shopperId = $session->store_get_shopper_id();
        $shopperName = $session->store_get_shopper_name();

        $products = $catalogue->getCatalogue();
        for ($i = 0; count($products) > $i; $i++) {
            $attributes = $catalogue->getAttributes($products[$i]['prod_id']);
            $products[$i]['attributes'] = $attributes;
            for ($k = 0; count($attributes) > $k; $k++) {
                $attributeValues = $catalogue->getAttributeValues($products[$i]['prod_id'], $attributes[$k]['id']);
                $products[$i]['attributes'][$k]['values'] = $attributeValues;
            }
        }

        return $this->render('catalogue/index.html.twig', [
            'products' => $products,
            'shopperId' => $shopperId,
            'shopperName' => $shopperName,
        ]);
    }
}
