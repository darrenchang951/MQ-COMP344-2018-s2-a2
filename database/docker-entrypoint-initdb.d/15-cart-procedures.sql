DELIMITER //

CREATE PROCEDURE GetCartItems(IN sessionID CHAR(32))
BEGIN
	SELECT 	CartProduct.CartProduct_id AS 'ID',
			CartProduct.Product_prod_id AS 'productID',
			Product.prod_name AS 'name',
			ProdPrices.PrPr_Price AS 'basePrice',
			CartProduct.CP_qty AS 'quantity'
	FROM CartProduct, Product, ProdPrices
	WHERE CartProduct.Session_id = sessionID
	AND CartProduct.Product_prod_id = Product.prod_id
	AND Product.prod_id = ProdPrices.PrPr_Prod_id;
END //

CREATE PROCEDURE `AddCartItem`(IN sessionID CHAR(32), IN productID INT, IN quantity INT)
BEGIN
	INSERT INTO CartProduct(Session_id, Product_prod_id, CP_qty)
	VALUES (sessionID, productID, quantity);

	SELECT LAST_INSERT_ID();
END //

CREATE PROCEDURE UpdateCartItemQuantity(IN CartItemID INT, IN NewQty INT, IN shouldAdd BOOLEAN)
BEGIN
	IF shouldAdd = False THEN
		UPDATE CartProduct
		SET CP_qty = NewQty
		WHERE CartProduct_id = CartItemID;
	ELSEIF shouldAdd = True THEN
		UPDATE CartProduct
        SET CP_Qty = CP_Qty + NewQty
        WHERE CartProduct_id = CartItemID;
	END IF;
END //

CREATE PROCEDURE DeleteCartItem(IN CartItemID INT)
BEGIN
	DELETE FROM CartProduct WHERE CartProduct_id = CartItemID;
END //



CREATE PROCEDURE GetCartItemAttributes(IN ProductID INT)
BEGIN
	SELECT 	Attribute.id AS 'attrID',
			Attribute.name,
			Attribute.Product_prod_id AS 'prodID'
	FROM Attribute
	WHERE Attribute.Product_prod_id = ProductID
	ORDER BY Attribute.name;
END //

CREATE PROCEDURE GetCartItemAttributeValues(IN AttrID INT, IN ProductID INT)
BEGIN
	SELECT 	AttributeValue.AttrVal_id AS 'attrValID',
			AttributeValue.AttrVal_Price AS 'priceChange',
			AttributeValue.AttrVal_Value AS 'value'
	FROM AttributeValue
	WHERE AttributeValue.AttrVal_Attr_id = AttrID
	AND AttributeValue.AttrVal_Prod_id = ProductID;
END //

CREATE PROCEDURE AddAttribute(IN cartProductID INT, AttributeID INT, IN AttrValID INT)
BEGIN
	INSERT INTO CartProductAttribute(Attribute_value_id, Attribute_id, CartProduct_CP_id)
	VALUES (AttrValID, AttributeID, cartProductID);
END //

CREATE PROCEDURE UpdateAttributeValue(IN newAttributeValueID INT, IN whichAttributeID INT, IN CartProduct_id INT)
BEGIN
	UPDATE CartProductAttribute
	SET Attribute_value_id = newAttributeValueID
	WHERE Attribute_id = whichAttributeID
	AND CartProduct_CP_id = CartProduct_id;
END //

CREATE PROCEDURE CheckAttributeValue(IN attributeID INT, IN valueID INT, IN cartProductID INT)
BEGIN
	SELECT count(*) AS 'count'
	FROM CartProductAttribute
	WHERE Attribute_id = attributeID
	AND Attribute_value_id = valueID
	AND CartProduct_CP_id = cartProductID;
END //

CREATE PROCEDURE FindInCarts(IN ProductID INT)
BEGIN
	SELECT CartProduct_id
    FROM CartProduct
    WHERE Product_prod_id = ProductID;
END //

CREATE PROCEDURE FindInMyCart(IN SessionID CHAR(32), IN AttributeID INT, IN AttributeValueID INT)
BEGIN
	SELECT CartProduct.CartProduct_id
	FROM CartProduct, CartProductAttribute
	WHERE CartProductAttribute.CartProduct_CP_id = CartProduct.CartProduct_id
	AND CartProduct.Session_id = SessionID
	AND CartProductAttribute.Attribute_id = AttributeID
	AND CartProductAttribute.Attribute_value_id = AttributeValueID;
END //

DELIMITER ;