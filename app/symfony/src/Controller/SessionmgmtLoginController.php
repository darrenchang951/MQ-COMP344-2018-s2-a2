<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Ecom\Sessionmgmt\Session;

class SessionmgmtLoginController extends AbstractController
{
    /**
     * @Route("/sessionmgmt/login", name="sessionmgmt_login")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        // Check if the client is a logged in shopper
        $sessionmgmt = new Session();
        $shopperId = $sessionmgmt->store_get_shopper_id();
        $shopperName = $sessionmgmt->store_get_shopper_name();

        return $this->render('sessionmgmt_login/index.html.twig', [
            'title' => 'Session Management Login',
            'shopperId' => $shopperId,
            'shopperName' => $shopperName,
        ]);
    }
}
