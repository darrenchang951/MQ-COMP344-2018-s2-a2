<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Ecom\Sessionmgmt\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class SessionmgmtLoginShopperController extends AbstractController
{
    /**
     * @Route("/sessionmgmt/login/shopper", name="sessionmgmt_login_shopper", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        // Obtain information from the user submitted form.
        $request = Request::createFromGlobals();
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        // Create object and check user submitted information.
        $login = new Session();
        $validLogin = $login->login($email, $password);
        return $this->redirectToRoute('sessionmgmt_login');
    }
}
