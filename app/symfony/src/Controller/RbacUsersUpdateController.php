<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Ecom\Rbac\Rbac;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Ecom\Sessionmgmt\Session;

class RbacUsersUpdateController extends AbstractController
{
    /**
     * @Route("/rbac/users/update", name="rbac_users_update", methods={"POST"})
     * @param Rbac $rbac
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Rbac $rbac, Request $request)
    {
        // Check if the client is a logged in shopper
        $sessionmgmt = new Session();
        $shopperId = $sessionmgmt->store_get_shopper_id();
        $shopperName = $sessionmgmt->store_get_shopper_name();

        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);
        if (!$rbac->hasPermission($uri)) {
            return $this->render('accessDenied.html.twig', [
                'shopperId' => $shopperId,
                'shopperName' => $shopperName,
            ]);
        }

        // Get client submitted form information
        $action = $request->request->get('action');
        $email = $request->request->get('email');

        // Create an array of permissions to be assigned for the role
        $re = '/(?:role-)(\d*)/m';
        $formData = $request->request->getIterator();
        $roles = [];
        foreach ($formData as $key => $value) {
            preg_match($re, $key, $match);
            if (count($match) > 0) {
                $roles[] = $match[1];
            }
        }

        // Manage users
        if ($action === 'delete') { // delete the user
            if (!$rbac->deleteUser($email)) {
                return $this->render('illegalOperation.html.twig', [
                    'shopperId' => $shopperId,
                    'shopperName' => $shopperName,
                    'message' => 'You cannot remove this user because the user is the last admin, or the user does not exist.',
                    'prevUri' => $this->generateUrl('rbac_users'),
                ]);
            }
        } elseif ($action === 'update') { // update role's permissions and description
            $rbac->deleteRolesFromUser($email);
            $rbac->addRolesToUser($email, $roles);
        }

        return $this->redirectToRoute('rbac_users');
    }
}
