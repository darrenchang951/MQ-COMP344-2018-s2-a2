<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Ecom\ShoppingCart\Cart;
use Symfony\Component\HttpFoundation\Response;

/**
 * Shopping cart subsystem written by "Shopping Cart Management" team
 * @package App\Controller
 */
class ApiCartController extends AbstractController
{
    /**
     * @Route("/api/cart", name="api_cart", methods={"POST"})
     * @param Request $request
     * @param Cart $cart
     * @param $role_name
     * @return JsonResponse|Response
     */
    public function index(Cart $cart)
    {
        // Create Object
        $response = new JsonResponse();

        // If `action` is not specified, encode and print cart contents as JSON
        if (!isset($_POST['action'])) {
            $str = json_encode($cart->getItems());
            $response->setJson($str);
            return $response;
        }

        // `data` must be present from this point forward
        if (!isset($_POST['data'])) die('Missing POST data!');

        $action = $_POST['action'];
        $data = json_decode($_POST['data']);

        // `itemid` must be present from this point forward
        if (!isset($data->itemid)) die('Missing item ID!');

        $item_id = intval($data->itemid, 10);

        // `item_id` must be an integer
        if ($item_id === 0) die('Item ID is not an integer!');

        // If `action` is not 'delete' or 'update_attr'...
        if ($action !== 'delete' && $action !== 'update_attr') {
            // ... `qty` must be present ...
            if (!isset($data->qty)) die('Missing quantity!');

            $qty = intval($data->qty, 10);

            // ... and `qty` must be an integer and positive
            if ($qty <= 0) die('Quantity is not positive (or a number)!');
        }

        // If 'action' is 'add', attributes/attribute values must be present
        if ($action === 'add' && !isset($data->attrs)) die('Missing attributes!');

        // If 'action' is 'update_attr', an attribute ID and its value ID must be present
        if ($action === 'update_attr' && (!isset($data->attrID) || !isset($data->attrValID))) die('Missing attribute ID and attribute value ID!');

        // Perform the appropriate action
        switch ($action) {
            case 'add':
                $cart->addItem($item_id, $data->attrs, $qty);
                break;
            case 'update_qty':
                $cart->updateQty($item_id, $qty);
                break;
            case 'update_attr':
                $cart->updateAttribute($data->itemid, $data->attrID, $data->attrValID);
                break;
            case 'delete':
                $cart->deleteItem($item_id);
                break;
            default:
                die('Action is not valid!');
                break;
        }
        return new Response('');
    }
}
