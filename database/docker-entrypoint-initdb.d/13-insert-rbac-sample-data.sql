insert into ShopperGroup(ShopGrp_id, ShopGrp_Name, ShopGrp_Description) values
(31, 'Basic', 'Basic shopper'),
(32, 'Pro', 'Monthly membership'),
(33, 'Elite', 'Yearly membership');

insert into Commands(Cmd_id, Cmd_name, Cmd_URL) values
#RBAC specific permissions - Admin
(10, 'View RBAC home page', '/rbac'),
(11, 'View users', '/rbac/users'),
(12, 'View roles', '/rbac/roles'),
(13, 'Obtain user information in JSON format', '/api/rbac/user'),
(14, 'Obtain role information in JSON format', '/api/rbac/role'),
(15, 'Add/delete/edit roles', '/rbac/roles/update'),
(16, 'Assign or revoke user''s roles', '/rbac/users/update'),
# The urls for these commands are based on RBAC team's assumption because other teams won't reply
# Catalogue manager
(17, 'Create product', '/product/add'),
(18, 'Edit product', '/product/edit'),
(19, 'Delete product', '/product/delete'),
(20, 'Create category', '/category/create'),
(21, 'Edit category', '/category/edit'),
(22, 'Delete category', '/category/delte'),

# Customer
(23, 'Checkout cart contents', '/checkout'),
(24, 'Submit support tickets', '/support-ticket/submit'),
(25, 'Change password', '/user/password/change'),
(26, 'Reset password', '/user/password/forgot'),
(27, 'Add to address book', '/user/address/add'),
(28, 'Edit address book entries', '/user/address/edit'),
(29, 'Remove from address book', '/user/address/delete'),
(30, 'Add reviews', '/review/add'),
(31, 'Edit reviews (self only)', '/review/edit'),
(32, 'Delete reviews (self only)', '/review/delete'),

# Customer service representative (CSR)
(33, 'View customer orders', '/orders'),
(34, 'Reply to customer queries', '/queries/reply'),
(35, 'Perform credit card reversals', '/user/credit-card-reversal'),
(36, 'Edit order records', '/orders/edit'),
(37, 'Add complaint records', '/complaint/add'),
(38, 'Edit complaint records', '/complaint/edit'),

# Warehouse staff (similar to CSR)

# Ratings/recommendations admin
(39, 'Edit reviews', '/review/edit'),
(40, 'Delete reviews', '/review/delete');

insert into AccessGroup(AG_id, AG_name, AG_desc) values
(20, 'Admin', 'A manager who has permission to every commands on the system.'),
(21, 'Catalogue Manager', 'An employee who has permissions related to shopkeeping.'),
(22, 'Customer', 'A customer who has basic permissions related to shopping.'),
(23, 'Customer Service Representative (CSR)', 'An employee who does customer service related tasks'),
(24, 'Warehouse Staff', 'An employee who ships customer orders.'),
(25, 'Rating and Recommendations Admin', 'An employee who reviews and manages customer comments.');

insert into AccessUserGroup(AUG_id, AUG_Shopper_id, AUG_AG_id) values
(50, 1, 20),
(51, 2, 21),
(52, 3, 22),
(53, 4, 23),
(54, 5, 24),
(55, 6, 25),
(56, 7, 22),
(57, 8, 22),
(58, 9, 22),
(59, 10, 22);

insert into AccessGroupCommands(AGC_id, AGC_AG_id, AGC_Cmd_id, AGC_desc) values
# Admin
(60, 20, 11, 'Admin can view all the users on the system.'),
(61, 20, 12, 'Admin can view all the roles on the system.'),
(62, 20, 13, 'Admin can obtain user information in JSON format.'),
(63, 20, 14, 'Admin can obtain role information in JSON format.'),
(64, 20, 15, 'Admin can manage roles.'),
(65, 20, 16, 'Admin can manage users'' roles.'),
(66, 20, 10, 'Admin can access RBAC home page.'),
(116, 20, 17, ''),
(93, 20, 18, ''),
(94, 20, 19, ''),
(95, 20, 20, ''),
(96, 20, 21, ''),
(97, 20, 22, ''),
(98, 20, 23, ''),
(99, 20, 24, ''),
(100, 20, 25, ''),
(101, 20, 26, ''),
(102, 20, 27, ''),
(103, 20, 28, ''),
(104, 20, 29, ''),
(105, 20, 30, ''),
(106, 20, 31, ''),
(107, 20, 32, ''),
(108, 20, 33, ''),
(109, 20, 34, ''),
(110, 20, 35, ''),
(111, 20, 36, ''),
(112, 20, 37, ''),
(113, 20, 38, ''),
(114, 20, 39, ''),
(115, 20, 40, ''),

# Catalogue manager
(67, 21, 17, ''),
(68, 21, 18, ''),
(69, 21, 19, ''),
(70, 21, 20, ''),
(71, 21, 21, ''),
(72, 21, 22, ''),

# Customer
(73, 22, 23, ''),
(74, 22, 24, ''),
(75, 22, 25, ''),
(76, 22, 26, ''),
(77, 22, 27, ''),
(78, 22, 28, ''),
(79, 22, 29, ''),
(80, 22, 30, ''),
(81, 22, 31, ''),
(82, 22, 32, ''),

# Customer service representative (CSR)
(83, 23, 33, ''),
(84, 23, 34, ''),
(85, 23, 35, ''),
(86, 23, 36, ''),
(87, 23, 37, ''),
(88, 23, 38, ''),

# Warehouse staff (similar to CSR)
(89, 24, 33, ''),
(90, 24, 36, ''),

# Ratings/recommendations admin
(91, 25, 39, ''),
(92, 25, 40, '');