<?php

namespace App\Ecom\Sessionmgmt;

use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\Driver\PDOException;
use App\Ecom\Dbpdo\CommonDb;

/**
 * Originally written by Les Bell
 * Class Session
 * @package App\Ecom\Sessionmgmt
 */
class Session
{
    private $db;

    function __construct()
    {
        // Create a database connection for the class
        $this->db = new CommonDb();
        $this->db = $this->db->conn();
    }

    public function check_credentials($email, $password)
    {
        $query = "SELECT shopper_id, sh_password FROM Shopper ";
        $query .= "WHERE sh_email = ?";

        $statement = $this->db->prepare($query);
        $statement->execute(array($email));

        $row = $statement->fetch();
        if ($row[0] > 0)
            // Code commended out because password in the db is not hashed
            if (password_verify($password, $row[1]))
                return ($row[0]);
            else
                return (0);
        else
            return (0);
    }

    public function login($email, $password)
    {
        $shopper_id = $this->check_credentials($email, $password);
        if ($shopper_id > 0) {
            // Start a session if one is not active. session_status doc: http://php.net/manual/en/function.session-status.php
            if (session_status() === 1) {
                session_start();
            }
            session_regenerate_id(FALSE);
            $sessid = session_id();
            $query = "INSERT INTO Session (id, Shopper_id) VALUES (?,?)";

            try {
                $statement = $this->db->prepare($query);
                $success = $statement->execute(array($sessid, $shopper_id));
            } catch (PDOException $ex) {
                error_log($ex->getMessage());
                die($ex->getMessage());
            }
            return (TRUE);
        } else {
            return (FALSE);
        }
    }

    public function logout()
    {
        $this->store_session_destroy();
        // Regenerate new id for the session
        session_regenerate_id(TRUE);
        session_destroy();
    }

    function store_get_shopper_id()
    {
        $query = "SELECT shopper_id FROM Session WHERE id = ?";
        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array(session_id()));
        } catch (PDOException $ex) {
            // error_log("PDO Exception in file $ex->getFile(), line $ex->getLine(): Code $ex->getCode() - $ex->getMessage()");
            return 0;
        }

        if ($statement->rowCount() === 0)
            return 0;

        return $statement->fetch(PDOConnection::FETCH_NUM)[0];
    }

    function store_get_shopper_name()
    {
        $shopperId = $this->store_get_shopper_id();
        $query = "SELECT sh_username FROM Shopper WHERE shopper_id = ?";
        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array($shopperId));
        } catch (PDOException $ex) {
            // error_log("PDO Exception in file $ex->getFile(), line $ex->getLine(): Code $ex->getCode() - $ex->getMessage()");
            return 0;
        }

        if ($statement->rowCount() === 0)
            return 0;

        return $statement->fetch(PDOConnection::FETCH_NUM)[0];
    }

    function store_session_read()
    {
        $session_id = session_id();
        $query = "SELECT data FROM Session WHERE id = ?";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array($session_id));
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }

        if ($statement->rowCount() === 1)
            return $statement->fetch()[0];

        return '';
    }

    function store_session_write($session_data)
    {
        $session_id = session_id();
        // First, see if that session already exists
        $query = "SELECT COUNT(*) FROM Session WHERE id = ?";
        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array($session_id));
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }
        $row = $statement->fetch();
        if ($row[0] > 0)
            $query = "UPDATE Session SET id = ?, data = ?";
        else
            $query = "REPLACE Session (id, data) VALUES (?,?)";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array($session_id, $session_data));
        } catch (PDOException $ex) {
            // Unreachable return statement if die before return.
            // die($ex->getMessage());
            return false;
        }

        return true;
    }

    function store_session_destroy()
    {
        $query = "DELETE FROM Session WHERE id = ?";
        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array(session_id()));
        } catch (PDOException $ex) {
            // Unreachable return statement if you die here!
            // die($ex->getMessage());
            return false;
        }
        return true;
    }

    function store_session_gc($gc_maxlife)
    {
        $query = "DELETE FROM Session WHERE time < NOW() - INTERVAL ? SECOND";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute($gc_maxlife);
        } catch (PDOException $ex) {
            // Unreachable return statement if die before return.
            die($ex->getMessage());
            return false;
        }

        return true;

    }
}
