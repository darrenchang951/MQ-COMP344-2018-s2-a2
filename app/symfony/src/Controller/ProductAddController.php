<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Ecom\Sessionmgmt\Session;
use Symfony\Component\HttpFoundation\Request;
use App\Ecom\Rbac\Rbac;

class ProductAddController extends AbstractController
{
    /**
     * @Route("/product/add", name="product_add")
     * @param Request $request
     * @param Session $session
     * @param Rbac $rbac
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, Session $session, Rbac $rbac)
    {
        // Check if the client is a logged in shopper
        $shopperId = $session->store_get_shopper_id();
        $shopperName = $session->store_get_shopper_name();

        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);

        if (!$rbac->hasPermission($uri)) {
            return $this->render('accessDenied.html.twig', [
                'shopperId' => $shopperId,
                'shopperName' => $shopperName,
            ]);
        }

        return $this->render('product_add/index.html.twig', [
            'controller_name' => 'ProductAddController',
            'shopperId' => $shopperId,
            'shopperName' => $shopperName,
        ]);
    }
}
