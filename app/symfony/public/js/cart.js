/**
 * @file Client-side JavaScript code for the shopping cart
 * @author William Orchiston <william.orchiston@students.mq.edu.au>
 */

/**
 * Anonymous function wrapping everything to enforce a 'this-file-only' scope
 */
(function() {
    /**
     * Array of all cart items
     */
    let items = [];



    // -------------------
    // AUXILIARY FUNCTIONS
    // -------------------



    /**
     * Display a price with its sign (-/+) before the dollar sign
     *
     * @param {number} price
     */
    function formatPrice(price)
    {
        return (price < 0) ? '-$' + price.substring(1) : '+$' + price;
    }

    /**
     * Calculate the total for a single cart item, with respect to its affecting attribute values
     *
     * @param {number} basePrice The price for that item without considering quantity or attributes
     * @param {number} qty The number of that type of item in the cart
     * @param {Array} attrs That item's attributes (and in turn values)
     *
     * @return {number} The effective (displayed) price of the item
     */
    function getItemPrice(basePrice, qty, attrs)
    {
        finalPrice = parseFloat(basePrice) * parseFloat(qty);

        for (attr of attrs) {
            for (value of attr.values) {
                if (value.selected) finalPrice += (parseFloat(value.priceChange, 10)) * parseFloat(qty);
            }
        }

        return (parseFloat(Math.round(finalPrice * 100) / 100).toFixed(2));
    }

    /**
     * Calculate the subtotal of all cart items
     *
     * @returns {number} The sum of all item prices in the shopping cart
     */
    function getSubtotal(items)
    {
        subtotal = 0.0;

        for (item of items) {
            // alert(JSON.stringify(item.attributes))
            subtotal += parseFloat(getItemPrice(item.basePrice, item.quantity, item.attributes));
        }

        return (Math.round(subtotal * 100) / 100).toFixed(2);
    }

    /**
     * Return the paramter string without the price at the end of it
     *
     * @param {string} itemName
     */
    String.prototype.stripPrice = function() {
        return (this.slice(0, this.lastIndexOf('('))).trim();
    };


    // -------------
    // AJAX REQUESTS
    // -------------



    /**
     * Fetch cart contents (as JSON) and populate `items`
     */
    function getCart()
    {
        $.ajax({
            url: '/api/cart',
            type: 'post',
            dataType: 'json',

            success: function(json) {
                items = JSON.parse(JSON.stringify(json));

                displayCart();
            }
        });
    }

    /**
     * Update cart contents via `itemid` and optional `qty`
     *
     * @param {string} action The type of action/AJAX request ('add', 'update_qty', 'update_attr', or 'delete')
     * @param {Object} data Supporting data to be sent along with the AJAX request; action-specific
     */
    function updateCart(action, data)
    {
        $.ajax({
            url: '/api/cart',
            type: 'post',
            data: {
                'action': action,
                'data': JSON.stringify(data)
            },

            success: function(response) {
                if (response) {
                    alert('There was an error updating your cart:\n' + response);

                    return;
                }

                getCart();  // Re-fetch cart contents on (successful) update
            }
        });

    }



    // ----------------
    // DOM MANIPULATION
    // ----------------



    /**
     * Display the contents of the `items` variable in the DOM, or a friendly message if empty
     */
    function displayCart()
    {
        $('#cart-items').empty();
        $('#cart-items-total').html(`(${items.length})`);
        $('#cart-subtotal').text(getSubtotal(items));

        if (items.length === 0) {
            $('#cart-items').html('<h5 class="title is-5">Your shopping cart is empty.</h5>');

            return;
        }

        for (item of items) {
            let curItem = $(`
                <div class="cart-item box has-background-grey-lighter" data-itemid=${item.ID} >
                    <div class="columns">
                        <div class="column is-one-fifth is-vcentered">
                            <img class="image" src="${item.image}" style="max-height: 150px; width: 150px;">
                            <button class="cart-item-delete button is-secondary is-fullwidth">Remove</button>
                        </div>
                        <div class="column">
                            <div class="is-grouped is-vcentered">
                                <h5 class="title is-5">${item.name} ($${getItemPrice(item.basePrice, item.quantity, item.attributes)})</h5>
                                <div class="columns is-vcentered">
                            </div>
                            <div class="cart-item-attributes"></div>
                        </div>
                    </div>
                </div>
            `);

            $('#cart-items').append(curItem);

            // Container for attributes
            let attrs = curItem.find('.cart-item-attributes');

            // Populate each item with attributes
            for (attribute of item.attributes) {
                let newAttr = $(`
                    <div class="columns is-vcentered">
                        <div class="column is-one-fifth has-text-right"><strong>${attribute.name}:</strong></div>
                        <div class="column"><div class="select"><select class="cart-item-attribute" data-attrid="${attribute.attrID}"></select></div></div>
                    </div>
                `);

                $(attrs).append(newAttr);

                // Container for attribute values
                let attrValues = $(newAttr).find(`[data-attrid="${attribute.attrID}"]`);

                // Populate each attribute with values
                for (value of attribute.values) {
                    attrValues.append(`
                        <option value="${value.attrValID}" ${(value.selected) ? 'selected' : ''}>
                            ${value.value} ${(parseFloat(value.priceChange) !== 0) ? '(' + formatPrice(value.priceChange) + ')' : ''}
                        </option>
                    `);
                }
            }

            // Have to do this last because Bulma is weird
            $(attrs).append(`
                <div class="columns is-vcentered">
                    <div class="column is-one-fifth has-text-right"><strong>Quantity:</strong></div>
                    <div class="column is-one-fifth"><input type="number" class="cart-item-qty input" value="${item.quantity}" min="1"></div>
                </div>
            `);
        }
    }



    // ----------------------
    // ELEMENT EVENT HANDLERS
    // ----------------------



    /**
     * When the 'Add to Cart' button is clicked...
     */
    $('.cart-item-add').click(function() {
        let thisItem = $(this).closest('.catalogue-item');
        let itemid = thisItem.data('itemid');
        let defaultQty = thisItem.find('.catalogue-item-qty').val();
        let attributes = [];

        // Capture the item's attributes, and the corresponding values of those attributes — both by ID
        thisItem.find('.catalogue-item-attribute').each(function() {
            attributes.push({
                attrID: $(this).data('attrid'),
                attrValID: $(this).val()
            });
        });

        updateCart('add', {
            'itemid': itemid,
            'attrs': attributes,
            'qty': defaultQty
        });
    });

    /**
     * When a cart item's quantity is updated...
     * Note: a slightly different event handler is required because delete buttons are generated dynamically.
     *
     * @see displayCart()
     */
    $(document).on('change', '.cart-item-qty', function() {
        let itemid = $(this).closest('.cart-item').data('itemid');
        let newQty = $(this).val();

        updateCart('update_qty', {
            'itemid': itemid,
            'qty': newQty
        });
    });

    /**
     * When a cart item's attribute is updated...
     */
    $(document).on('change', 'select.cart-item-attribute', function() {
        let itemid = $(this).closest('.cart-item').data('itemid');
        let attrID = $(this).data('attrid');
        let attrValID = $(this).val();

        updateCart('update_attr', {
            'itemid': itemid,
            'attrID': attrID,
            'attrValID': attrValID
        });
    });

    /**
     * When the 'Remove from Cart' button is clicked...
     * Note: a slightly different event handler is required because delete buttons are generated dynamically.
     *
     * @see displayCart()
     */
    $(document).on('click', '.cart-item-delete', function() {
        // if (!confirm('Are you sure you want to delete this item?')) return;

        let itemid = $(this).closest('.cart-item').data('itemid');

        updateCart('delete', {
            'itemid': itemid
        });
    });

    /**
     * Immediately fetch shopping cart items on page load
     */
    getCart();
})();