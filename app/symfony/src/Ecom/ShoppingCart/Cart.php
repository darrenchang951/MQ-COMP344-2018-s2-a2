<?php

namespace App\Ecom\ShoppingCart;

use App\Ecom\Dbpdo\CommonDb;
use Doctrine\DBAL\Driver\PDOConnection;
use App\Ecom\Sessionmgmt\Session;

/**
 * Describes a single user's shopping cart, and provides methods for modifying its contents
 * The class is modified by Darren
 * @author William Orchiston <william.orchiston@students.mq.edu.au>
 * @access public
 */
class Cart
{
    private $session_id;
    private $cart_id;
    private $db;

    public function __construct()
    {
        // Create a database connection for the class
        $this->db = new CommonDb();
        $this->db = $this->db->conn();
        $session = new Session();
        $this->session_id = ($session->store_get_shopper_id() === 0) ? session_id() : $session->store_get_shopper_id();
    }

    /**
     * Return the contents of the requesting customer's shopping cart — whether they are logged in or otherwise
     *
     * @return Array
     */
    public function getItems()
    {
        $stmt = $this->db->prepare('CALL GetCartItems(:SESSION_ID)');
        $stmt->bindParam(':SESSION_ID', $this->session_id);
        $stmt->execute();
        $items = $stmt->fetchAll(PDOConnection::FETCH_ASSOC);
        $stmt->closeCursor();

        // Add attributes and attribute values to each item fetched
        foreach ($items as $key => $item) {
            $items[$key]['attributes'] = $this->getItemAttributes($item['productID'], $item['ID']);
            $items[$key]['image'] = $this->generateItemImageURL($items[$key]['name'], $items[$key]['attributes']);
        }

        return $items;
    }

    /**
     * Return the URL for a item image from a specified host, given its name and attributes
     *
     * @param int $item_name The name of the item
     * @param Array $attrs An associative array of attributes and corresponding values
     *
     * @return string
     */
    private function generateItemImageURL($item_name, $attrs)
    {
        // Base URL (hotlinking is bad m'kay)
        $url = 'http://angrypig.com.au/comp344/app/images/' . strtolower($item_name);
        $url .= (!is_numeric(substr($url, -1, 1))) ? '1-' : '-'; // Append a trailing '1' if not present

        foreach ($attrs as $attr) {
            foreach ($attr['values'] as $value) {
                // If the current attribute value is 'selected'...
                if ($value['selected']) {
                    // ... replace non-alphanumeric characters in its value with with '_' and append to the URL
                    $url .= preg_replace("/\W/", '_', strtolower($value['value'])) . '-';
                }
            }
        }

        return substr_replace($url, '.png', strlen($url) - 1, 1);
    }

    /**
     * Return a cart item's attributes, as well as their corresponding values, which may be 'selected'
     *
     * @param int $product_id The ID of the product those attributes/attribute values correspond to
     * @param int $item_id The ID of the cart item to fetch attributes/attribute values for
     * @see Cart::isAttributeValueSelected() To see how attribute values are marked as 'selected'
     *
     * @return Array
     */
    private function getItemAttributes($product_id, $item_id)
    {
        $stmt = $this->db->prepare('CALL GetCartItemAttributes(:PRODUCT_ID)');
        $stmt->bindParam(':PRODUCT_ID', $product_id);
        $stmt->execute();
        $attrs = $stmt->fetchAll(PDOConnection::FETCH_ASSOC);
        $stmt->closeCursor();

        foreach ($attrs as $key => $attr) {
            $values = &$attrs[$key]['values']; // Shallow copy; `$values` is a pointer
            $values = $this->getItemAttributeValues($attr['attrID'], $product_id);

            // Mark selected attribute values as 'selected', as is specified in CartProductAttribute
            foreach ($values as $_key => $value)
                $values[$_key]['selected'] = $this->isAttributeValueSelected($attr['attrID'], $value['attrValID'], $item_id);
        }

        return $attrs;
    }

    /**
     * Return an attribute's values
     *
     * @param int $attr_id The ID of the attribute in question
     * @param int $product_id The ID of the product that attribute corresponds to
     *
     * @return Array An associative array of attributes and corresponding values
     */
    private function getItemAttributeValues($attr_id, $product_id)
    {
        $stmt = $this->db->prepare('CALL GetCartItemAttributeValues(:ATTR_ID, :PRODUCT_ID)');
        $stmt->bindParam(':ATTR_ID', $attr_id);
        $stmt->bindParam(':PRODUCT_ID', $product_id);
        $stmt->execute();
        $values = $stmt->fetchAll(PDOConnection::FETCH_ASSOC);
        $stmt->closeCursor();

        return $values;
    }

    /**
     * Return true if a provided attribute value is 'selected' for the current cart item, false otherwise
     *
     * @param int $attr_id The ID of the attribute
     * @param int $attr_val_id The ID of the attribute value
     * @param int $item_id The ID of the current cart item
     *
     * @return bool
     */
    private function isAttributeValueSelected($attr_id, $attr_val_id, $item_id)
    {
        $stmt = $this->db->prepare('CALL CheckAttributeValue(:ATTR_ID, :ATTR_VAL_ID, :ITEM_ID)');
        $stmt->bindParam(':ATTR_ID', $attr_id);
        $stmt->bindParam(':ATTR_VAL_ID', $attr_val_id);
        $stmt->bindParam(':ITEM_ID', $item_id);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        $stmt->closeCursor();

        return ($count > 0);
    }

    /**
     * Insert a new cart item
     * N.B. If the item already exists, its quantity will be updated instead (by adding `$default_qty` to it)
     *
     * @param int $product_id The ID of the product to be added
     * @param Array $default_attrs An associative array of attributes and corresponding values
     * @param int $default_qty The initial quantity of the item in the cart
     *
     * @return void
     */
    public function addItem($product_id, $default_attrs, $default_qty = 1)
    {
        $item_id = $this->findItem($product_id, $default_attrs);

        if ($item_id !== 0) {
            $this->updateQty($item_id, $default_qty, true);

            return;
        }

        $stmt = $this->db->prepare('CALL AddCartItem(:SESSION_ID, :PRODUCT_ID, :DEFAULT_QTY)');
        $stmt->bindParam(':SESSION_ID', $this->session_id);
        $stmt->bindParam(':PRODUCT_ID', $product_id);
        $stmt->bindParam(':DEFAULT_QTY', $default_qty);
        $stmt->execute();
        $item_id = $stmt->fetchColumn();
        $stmt->closeCursor();

        $this->addItemAttribute($item_id, $default_attrs);
    }

    /**
     * Return the ID of a shopping cart item if its product ID is found, or 0 otherwise
     *
     * @param int $product_id The ID of the product to be found
     * @param Array $attrs An associative array of attributes and corresponding values
     *
     * @return int
     */
    private function findItem($product_id, $attrs)
    {
        $stmt = $this->db->prepare('CALL FindInCarts(:PRODUCT_ID)');
        $stmt->bindParam(':PRODUCT_ID', $product_id);
        $stmt->execute();
        $item_id = $stmt->fetchColumn();
        $stmt->closeCursor();

        // An item of the same 'type' doesn't exist; no need to continue
        if (!$item_id) return 0;

        $stmt = $this->db->prepare('CALL FindInMyCart(:SESSION_ID, :ATTR_ID, :ATTR_VAL_ID)');
        $stmt->bindParam(':SESSION_ID', $this->session_id);

        foreach ($attrs as $attr) {
            $stmt->bindParam(':ATTR_ID', $attr->attrID);
            $stmt->bindParam(':ATTR_VAL_ID', $attr->attrValID);
            $stmt->execute();
            $item_id = $stmt->fetchColumn();
            $stmt->closeCursor();

            if (!$item_id) break;
        }

        return ($item_id) ? $item_id : 0;
    }

    /**
     * Insert attributes (and corresponding values) for the cart item being added
     *
     * @param int $item_id The ID of the current cart item
     * @param Array $attrs An associative array of attributes and corresponding values
     *
     * @return void
     */
    public function addItemAttribute($item_id, $attrs)
    {
        $stmt = $this->db->prepare('CALL AddAttribute(:ITEM_ID, :ATTR_ID, :ATTR_VAL_ID)');
        $stmt->bindParam(':ITEM_ID', $item_id);

        foreach ($attrs as $attr) {
            $stmt->bindParam(':ATTR_ID', $attr->attrID);
            $stmt->bindParam(':ATTR_VAL_ID', $attr->attrValID);
            $stmt->execute();
        }

        $stmt->closeCursor();
    }

    /**
     * Update the quantity of a cart item
     *
     * @param int $item_id The ID of the item to be updated
     * @param int $new_qty The new quantity of the item
     * @param bool $add_to Specifies whether the new quantity should be added to the old quantity
     *
     * @return void
     */
    public function updateQty($item_id, $new_qty, $add_to = false)
    {
        $add_to = (int)$add_to;

        $stmt = $this->db->prepare('CALL UpdateCartItemQuantity(:ITEM_ID, :NEW_QTY, :ADD_TO)');
        $stmt->bindParam(':ITEM_ID', $item_id);
        $stmt->bindParam(':NEW_QTY', $new_qty);
        $stmt->bindParam(':ADD_TO', $add_to);
        $stmt->execute();
        $stmt->closeCursor();
    }

    /**
     * Update the attribute (value) of a cart item
     *
     * @param int $item_id The ID of the item the attribute is assigned to
     * @param int $attr_id The ID of the attribute to be changed
     * @param int $new_attr_val_id The ID of the new attribute value
     *
     * @return void
     */
    public function updateAttribute($item_id, $attr_id, $new_attr_val_id)
    {
        $stmt = $this->db->prepare('CALL UpdateAttributeValue(:NEW_ATTR_VAL_ID, :ATTR_ID, :ITEM_ID)');
        $stmt->bindParam(':NEW_ATTR_VAL_ID', $new_attr_val_id);
        $stmt->bindParam(':ATTR_ID', $attr_id);
        $stmt->bindParam(':ITEM_ID', $item_id);
        $stmt->execute();
        $stmt->closeCursor();
    }

    /**
     * Delete a cart item
     *
     * @param int $item_id The ID of the item to be deleted
     *
     * @return void
     */
    public function deleteItem($item_id)
    {
        $stmt = $this->db->prepare('CALL DeleteCartItem(:ITEM_ID)');
        $stmt->bindParam(':ITEM_ID', $item_id);
        $stmt->execute();
        $stmt->closeCursor();
    }
}