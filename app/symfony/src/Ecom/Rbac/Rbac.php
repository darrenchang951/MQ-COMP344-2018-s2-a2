<?php

namespace App\Ecom\Rbac;

use App\Ecom\Dbpdo\CommonDb;

/**
 * By Darren And Nazir
 * Class Rbac
 * @package App\Ecom\Rbac
 */
class Rbac
{
    private $db;

    function __construct()
    {
        // Create a database connection for the class
        $this->db = new CommonDb();
        $this->db = $this->db->conn();
    }

    /**
     * @return array - all the users available on the system
     */
    function getUsers()
    {
        $sql = "SELECT `shopper_id`, `sh_username`, `sh_email` FROM `Shopper`";
        $statement = $this->db->query($sql);
        $users = $statement->fetchAll();
        $users_array = [];
        for ($x = 0; $x < count($users); $x++) {
            $users_array[] = array(
                "id" => $users[$x][0],
                "name" => $users[$x][1],
                "email" => $users[$x][2],
            );
        }
        return $users_array;
    }

    /**
     * @return array - all the available roles on the system
     */
    function getRoles()
    {
        $sql = "SELECT `AG_id`, `AG_name`, `AG_desc` from AccessGroup";
        $statement = $this->db->query($sql);
        $roles = $statement->fetchAll();
        $roles_array = [];
        for ($x = 0; $x < count($roles); $x++) {
            $roles_array[] = array(
                "id" => $roles[$x][0],
                "name" => $roles[$x][1],
                "desc" => $roles[$x][2],
            );
        }
        return $roles_array;
    }

    /**
     * @return array - all the permissions available on the system
     */
    function getPermissions()
    {
        $sql = "SELECT `Cmd_id`, `Cmd_name`, `Cmd_URL` from Commands";
        $statement = $this->db->query($sql);
        $permissions = $statement->fetchAll();
        $permissions_array = [];
        for ($x = 0; $x < count($permissions); $x++) {
            $permissions_array[] = array(
                "id" => $permissions[$x][0],
                "name" => $permissions[$x][1],
                "url" => $permissions[$x][2],
            );
        }
        return $permissions_array;
    }

    /**
     * @param $perm - command url
     * @return bool - true if the user has permission, else false
     */
    function hasPermission($perm)
    {
        $sql = 'select sh_username, Cmd_name, Session.id
                from Shopper, AccessGroup, Commands, AccessUserGroup, AccessGroupCommands, Session
                where Session.Shopper_id = Shopper.shopper_id and
                AccessGroup.AG_id = AccessGroupCommands.AGC_AG_id and
                Commands.Cmd_id = AccessGroupCommands.AGC_Cmd_id and 
                AccessGroup.AG_id = AccessUserGroup.AUG_AG_id and
                Shopper.shopper_id = AccessUserGroup.AUG_Shopper_id and
                Session.id = ? and Cmd_URL = ?';

        $stmt = $this->db->prepare($sql);
        // We take the client's session id and check it against the database.
        $stmt->execute([session_id(), $perm]);
        $temps = $stmt->fetchAll();
        if (count($temps) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the specified user's permissions in an array
     * @param $email - the user's email
     * @return array - An 2D array of permissions, detailing id, name, and url
     */
    function getUserPermissions($email)
    {

        $sql = 'select Cmd_id, Cmd_name, Cmd_URL
				from Shopper, AccessGroup, Commands, AccessUserGroup, AccessGroupCommands
				where AccessGroup.AG_id = AccessGroupCommands.AGC_AG_id and
				Commands.Cmd_id = AccessGroupCommands.AGC_Cmd_id and 
				AccessGroup.AG_id = AccessUserGroup.AUG_AG_id and
				Shopper.shopper_id = AccessUserGroup.AUG_Shopper_id and
				sh_email = ?';

        $stmt = $this->db->prepare($sql);
        $stmt->execute([$email]);
        $permissions = $stmt->fetchAll();

        $permissions_array = [];
        for ($x = 0; $x < count($permissions); $x++) {
            $permissions_array[] = array(
                "id" => $permissions[$x][0],
                "name" => $permissions[$x][1],
                "url" => $permissions[$x][2],
            );
        }
        return $permissions_array;
    }

    /**
     * @param $role - the name of the role
     * @return array - contains the role's permissions and description
     */
    function getRolePermissions($role)
    {
        $sql = "SELECT DISTINCT Cmd_id, Cmd_name, Cmd_URL, !ISNULL(AGC_id) hasPermission
                FROM (
                  SELECT AG_desc, AGC_id, AGC_Cmd_id
                  FROM AccessGroup
                  LEFT JOIN AccessGroupCommands AGC on AccessGroup.AG_id = AGC.AGC_AG_id
                  where AG_name = ?) t
                right join Commands on Commands.Cmd_id = t.AGC_Cmd_id;";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$role]);
        $permissions = $stmt->fetchAll();

        $permissions_array = Array();
        for ($x = 0; $x < count($permissions); $x++) {
            $permissions_array[] = array(
                "id" => $permissions[$x][0],
                "name" => $permissions[$x][1],
                "url" => $permissions[$x][2],
                "hasPermission" => $permissions[$x][3] === '1' ? true : false, //change string 0/1 to boolean
            );
        }
        return $permissions_array;
    }

    /**
     * @param $email - the user email
     * @return array - the roles details, including which roles the user is and is not
     */
    function getUserRoles($email)
    {
        $sql = 'SELECT AG_id, AG_name, AG_desc, !ISNULL(AUG_id) hasPermission
                FROM
                (SELECT *
                FROM Shopper
                left join AccessUserGroup G on Shopper.shopper_id = G.AUG_Shopper_id
                WHERE sh_email = ?) t
                right join AccessGroup on t.AUG_AG_id = AccessGroup.AG_id;';

        $stmt = $this->db->prepare($sql);
        $stmt->execute([$email]);
        $roles = $stmt->fetchAll();

        $roles_array = [];
        for ($x = 0; $x < count($roles); $x++) {
            $roles_array[] = array(
                "id" => $roles[$x][0],
                "name" => $roles[$x][1],
                "desc" => $roles[$x][2],
                "isRole" => $roles[$x][3] === '1' ? true : false, //change string 0/1 to boolean
            );
        }
        return $roles_array;
    }

    /**
     * @param $roleName - the name of the role
     * @param $roleDesc - description of the role
     * @return bool - true if successful, false if error occurs
     */
    function addRole($roleName, $roleDesc)
    {

        $sql = 'INSERT INTO AccessGroup (AG_name, AG_desc)
                VALUES (?, ?)';
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute([$roleName, $roleDesc])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $roleNmae - the name of the role
     * @return bool - true if successful, false if error occurs
     */
    function deleteRole($roleName)
    {
        $sql = 'delete from AccessGroup
		where AG_name = ?';
        $stmt = $this->db->prepare($sql);
        // If the role intended to delete is the last admin, return false.
        if ($this->isLastAdminRole($roleName)) {
            return false;
        } else {
            return $stmt->execute([$roleName]);
        }
    }

    /**
     * @param $roleName
     * @return int 0 if not found, role Id if found
     */
    function getRoleId($roleName)
    {
        $sql = 'select AG_id 
				from AccessGroup
				where AG_name = ?';

        $stmt = $this->db->prepare($sql);
        $stmt->execute([$roleName]);
        $role = $stmt->fetchAll();
        if (count($role) > 0) {
            return $role[0][0];
        }

        return 0;
    }

    /**
     * @param $roleId
     * @return array
     */
    function getRoleInfo($roleId)
    {
        $sql = 'select AG_id, AG_name, AG_desc 
				from AccessGroup
				where AG_id = ?';

        $stmt = $this->db->prepare($sql);
        $stmt->execute([$roleId]);
        $info = $stmt->fetchAll();
        $info_array = [];
        if (0 < count($info)) {
            $info_array = array(
                "id" => $info[0][0],
                "name" => $info[0][1],
                "desc" => $info[0][2],
            );
        }

        return $info_array;
    }

    /**
     * Add an array of permissions to the role. It's not neccessary to check if the permission ID exist, because if it
     * doesn't, the database will reject the insert anyways.
     * @param $role - name of the role
     * @param $permissions - a array of permissions (id) to be added for the role
     * @return bool - true if successful, false if error occurs
     */
    function addPermissionsToRole($roleName, $permissions)
    {
        // Get the ID of the role
        $roleId = $this->getRoleId($roleName);

        // If the role is valid and has an id, add the permissions to the role
        if ($roleId > 0) {
            foreach ($permissions as $permission) {
                $sql = 'INSERT INTO AccessGroupCommands(AGC_AG_id, AGC_Cmd_id) VALUES
				 (?,?)';
                $stmt = $this->db->prepare($sql);
                $stmt->execute([$roleId, $permission]);
            }
            return true;
        }
        return false;
    }

    /**
     * Delete all the permissions from role
     * @param $roleName
     * @return bool - true if successful, false if error occurs
     */
    function deletePermissionsFromRole($roleName)
    {
        // Get the ID of the role
        $roleId = $this->getRoleId($roleName);

        // If the role is valid and has an id, remove all permissions to the role
        if ($roleId > 0) {
            $sql = 'delete from AccessGroupCommands 
				where AGC_AG_id=?';

            $stmt = $this->db->prepare($sql);
            return $stmt->execute([$roleId]);
        }
        return false;
    }

    /**
     * @param $roleName - the name of the role
     * @param $roleDesc - Description of the role
     * @return bool - true if successful, false if error occurs
     */
    function updateRoleDesc($roleName, $roleDesc)
    {
        // Get the ID of the role
        $roleId = $this->getRoleId($roleName);
        if ($roleId > 0) {
            $sql = 'UPDATE `AccessGroup` SET `AG_desc` = ? WHERE `AG_id` = ?';
            $stmt = $this->db->prepare($sql);
            return $stmt->execute([$roleDesc, $roleId]);
        }
        return false;
    }

    /**
     * @param $userEmail
     * @return bool - true if successful, false if error occurs
     */
    function deleteUser($userEmail)
    {
        $sql = 'DELETE FROM Shopper WHERE sh_email = ?';
        $stmt = $this->db->prepare($sql);
        if ($this->isLastAdminUser($userEmail)) {
            return false;
        } else {
            return $stmt->execute([$userEmail]);
        }
    }


    /**
     * @param $userEmail
     * @return int - 0 if not found, role Id if found
     */
    function getUserId($userEmail)
    {
        $sql = "SELECT shopper_id FROM Shopper WHERE sh_email = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$userEmail]);
        $user = $stmt->fetchAll();
        if (count($user) > 0) {
            return $user[0][0];
        }
        return 0;
    }

    /**
     * Delete all roles form the user
     * @param $userEmail
     * @return bool - true if success, else false
     */
    function deleteRolesFromUser($userEmail)
    {
        $userId = $this->getUserId($userEmail);
        $sql = 'DELETE FROM AccessUserGroup WHERE AUG_Shopper_id = ?;';
        $stmt = $this->db->prepare($sql);
        return $stmt->execute([$userId]);
    }

    /**
     * Add an array of roles to the user. It's not neccessary to check if the role ID exist, because if it
     * doesn't, the database will reject the insert anyways.
     * @param $user
     * @param $roles - an array of roles (id) to be added for the user
     * @return bool - true if success, else false
     */
    function addRolesToUser($user, $roles)
    {
        $userId = $this->getUserId($user);
        if ($userId > 0) {
            foreach ($roles as $role) {
                $sql = 'INSERT INTO AccessUserGroup(AUG_Shopper_id, AUG_AG_id) VALUES
                        (?, ?)';
                $stmt = $this->db->prepare($sql);
                $stmt->execute([$userId, $role]);
            }
            return true;
        }
        return false;
    }

    /**
     * @param $roleName
     * @return bool - true if the given role is the last admin
     */
    function isLastAdminRole($roleName)
    {
        $roleId = $this->getRoleId($roleName);
        $sql = 'SELECT *
                FROM (SELECT shopper_id, AccessGroup.AG_id, count(DISTINCT AGC_Cmd_id) `Num`
                      FROM Shopper,
                           AccessUserGroup,
                           AccessGroup,
                           AccessGroupCommands
                      where Shopper.shopper_id = AccessUserGroup.AUG_Shopper_id
                        and AccessUserGroup.AUG_AG_id = AccessGroup.AG_id
                        and AccessGroup.AG_id = AccessGroupCommands.AGC_AG_id
                      group by AccessGroup.AG_id, shopper_id) AS shooper_cmd_count,
                     (SELECT count(DISTINCT Cmd_id) as `TotalCmdNum` FROM Commands) as Cmd
                WHERE TotalCmdNum = Num';
        $stmt = $this->db->query($sql);
        $admins = $stmt->fetchAll();
        if (count($admins) == 1 and $admins[0][1] == $roleId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $userEmail
     * @return bool - true if only one admin user left in the system. Admin is defined as a role who has every permissions
     * available in the system
     */
    function isLastAdminUser($userEmail)
    {
        $userId = $this->getUserId($userEmail);
        $sql = 'SELECT *
                FROM (SELECT shopper_id, AccessGroup.AG_id, count(DISTINCT AGC_Cmd_id) `Num`
                      FROM Shopper,
                           AccessUserGroup,
                           AccessGroup,
                           AccessGroupCommands
                      where Shopper.shopper_id = AccessUserGroup.AUG_Shopper_id
                        and AccessUserGroup.AUG_AG_id = AccessGroup.AG_id
                        and AccessGroup.AG_id = AccessGroupCommands.AGC_AG_id
                      group by AccessGroup.AG_id, shopper_id) AS shooper_cmd_count,
                     (SELECT count(DISTINCT Cmd_id) as `TotalCmdNum` FROM Commands) as Cmd
                WHERE TotalCmdNum = Num;';
        $stmt = $this->db->query($sql);
        $admins = $stmt->fetchAll();
        if (count($admins) == 1 and $admins[0][0] == $userId) {
            return true;
        } else {
            return false;
        }
    }
}
