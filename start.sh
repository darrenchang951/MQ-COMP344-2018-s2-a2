#!/bin/bash
# 1. Check if .env file exists
if [ -e .env ]; then
    source .env
else
    echo "Continuing with default options defined in ./docker-compose.yml"
fi

# 2. Download the latest version of nginx.tmpl
curl https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl > nginx.tmpl

# 3. Create Docker network
docker network create $NETWORK $NETWORK_OPTIONS

# 4. Start the application services
docker-compose up -d

# 5 Start letsencrypt-nginx-proxy-companion
docker-compose -f docker-compose-letsencrypt.yml up -d

# 4. Download Symfony dependencies
docker exec -ti comp344_store_app /usr/src/setup/install-symfony.sh

exit 0
