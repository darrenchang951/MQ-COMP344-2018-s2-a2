<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Ecom\Rbac\Rbac;

class ApiRbacUserController extends AbstractController
{
    /**
     * @Route("/api/rbac/user/{user_email}", name="api_rbac_user")
     * @param Request $request
     * @param Rbac $rbac
     * @param $user_email
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, Rbac $rbac, $user_email)
    {
        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);
        // remove user_email from the url and the leading /
        $uri = str_replace("/" . $user_email, "", $uri);

        // Create Object
        $response = new JsonResponse();

        if ($rbac->hasPermission($uri)) {
            // Get user's permissions
            $permissions = $rbac->getUserPermissions($user_email);
            $roles = $rbac->getUserRoles($user_email);
            $response->setData(array(
                'permissions' => $permissions,
                'roles' => $roles,
                'user' => $user_email,
            ));
            return $response;
        } else {
            $response->setData(array(
                'permissions' => "",
                'roles' => "",
                'user' => $user_email,
            ));
            return $response;
        }
    }
}
