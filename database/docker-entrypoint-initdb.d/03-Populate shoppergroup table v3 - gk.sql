-- A default Primary Key of 1 and a default ShopperGroup of 'BASIC' is used. Please change to suite production database
INSERT INTO ShopperGroup VALUES (1,'B-C Low Volume','B-C. Low Volume/Price over the Fin Year. One off or few off.');
INSERT INTO ShopperGroup VALUES (2,'B-C Medium Volume','B-C. Medium Volume/Price over the Fin Year. Reasonably regular purchaser.');
INSERT INTO ShopperGroup VALUES (3,'B-C High Volume','B-C. High Volume/Price over the Fin Year. Regular Purchaser.');
INSERT INTO ShopperGroup VALUES (4,'B-B Low Volume','B-B. Low Volume/Price over the Fin Year – Account Customer. One off or few off.');
INSERT INTO ShopperGroup VALUES (5,'B-B Medium Volume','B-B. Medium Volume/Price over the Fin Year – Account Customer. Reasonably regular purchaser.');
INSERT INTO ShopperGroup VALUES (6,'B-B High Volume','B-B. High Volume/Price over the Fin Year – Account Customer. Regular Purchaser.');
INSERT INTO ShopperGroup VALUES (7,'B-G Low Volume','B-G. Low Volume/Price over the Fin Year – Contract Customer. One off or few off.');
INSERT INTO ShopperGroup VALUES (8,'B-G Medium Volume','B-G. Medium Volume/Price over the Fin Year – Contract Customer. Reasonably regular purchaser.');
INSERT INTO ShopperGroup VALUES (9,'B-G High Volume','B-G. High Volume/Price over the Fin Year – Contract Customer. Regular Purchaser.');