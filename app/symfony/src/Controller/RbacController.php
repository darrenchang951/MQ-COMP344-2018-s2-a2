<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Ecom\Rbac\Rbac;
use App\Ecom\Sessionmgmt\Session;

class RbacController extends AbstractController
{
    /**
     * @Route("/rbac", name="rbac")
     * @param Request $request
     * @param Rbac $rbac
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, Rbac $rbac)
    {
        // Check if the client is a logged in shopper
        $sessionmgmt = new Session();
        $shopperId = $sessionmgmt->store_get_shopper_id();
        $shopperName = $sessionmgmt->store_get_shopper_name();

        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);

        if (!$rbac->hasPermission($uri)) {
            return $this->render('accessDenied.html.twig', [
                'shopperId' => $shopperId,
                'shopperName' => $shopperName,
            ]);
        }

        return $this->render('rbac/index.html.twig', [
            'title' => 'RBAC Management page',
            'role_management' => '/rbac/roles',
            'user_management' => '/rbac/users',
            'shopperId' => $shopperId,
            'shopperName' => $shopperName,
        ]);
    }
}
