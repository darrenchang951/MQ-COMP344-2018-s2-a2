# MQ-COMP344-2018-s2-a2

Assignment 2 for COMP344 (e-commerce technology) at Macquarie University, semester 2.

# Getting started
## Using Docker 
- Build and start the application.
```bash
./start.sh
```

### Development
- If your server is not reachable by Let's Encrypt, localhost for example, you need to bind web container port to the 
host. This can be done in `docker-compose.yml` by uncommenting the lines that binds web:80 to localhost:8080.
```bash
docker-compose up -d
```

- Use this command if you need to restart your application. 
```bash
docker-compose down; docker-compose up -d
```

- Create database SQL dump on the host
```bash
docker exec lol_journal_db sh -c 'exec mysqldump -u comp344_store_admin -p"password" comp344_store' > /some/path/on/your/host/dump.sql
```

- Sample user login credential can be found in this [file](database/docker-entrypoint-initdb.d/05-Populate%20shaddr%20table%20V2%20-%20gk.sql)

## Using symfony test web server
This only starts a web server for the application. You need to set up your own database.
- Start the web server. Navigate to `/app/symfony/` and execute
`php bin/console server:run`

## Using WAMP or whatever stacks
- Set up PHP and php-fpm.
- Set your webserver root path to `/app/symfony/public`. See 
<https://symfony.com/doc/current/setup/web_server_configuration.html> for more details on web server configurations.
- Set up your database. 

# Using existing data
If you have an SQL dump, you can put it inside `database/docker-entrypoint-initdb.d` folder so it's executed when the 
database container
starts for the first time.

# Directory/file explanations
- `app` -- application
- `app/symfony` -- symfony framework directory (mounted to docker container). This is where you do your web dev.
- `webserver` -- Web server configuration for the docker image.
- `database/docker-entrypoint-initdb.d` -- Place your SQL dump inside this directory to import your existing data when 
you start the container.
- `database/` -- contains MySQL configuration files.
- `*postman_collection.json` -- Behavior testing. You can run it using `newman run mycollection.json` or using Postman. 
newman can be installed using `npm install -g newman`.
client.
## Integration tips
These are the only places you need to touch.
- `app/symfony/src/Controller` -- contains the controllers
- `app/symfony/src/Ecom` -- Create a folder for your subsystem, and place your .php files in there.
- `app/symfony/config/services.yaml` -- Added your service in here.
- `app/symfony/templates` -- twig templates.

You can use command to create new routes automatically by using the below command
```bash
# Allocate a interactive pseudo-TTY so you can run bash inside your container 
docker exec -ti comp344_store_app bash
# While inside the container, you can create new controllers 
php bin/console make:controller testEngine
```

# Testing
## Functional test
Nothing available yet

## Postman Behavior Driven Development (Postman BDD)
You can run the test using the below command. The test is not very sophisticated. 
```bash
newman run mycollection.json
```

# Application 
Hosts:
- local prod: <localhost:80>
- live-demo prod: <https://comp.darrenhub.me>

## Subsystem URLs (Note, Our group is only responsible RBAC subsystem.)
| **RBAC URLs**              | HTTP method | Response         | Description                           |
|----------------------------|-------------|------------------|---------------------------------------|
| /                          | GET         | text/html        | Index page where you can access other demo subsystems |
| /rbac                      | GET         | text/html        | The main page for role-based access control. |
| /rbac/roles                | GET         | text/html        | A page where the admin can Create/Update/Delete roles |
| /rbac/users                | GET         | text/html        | A page where the admin can modify user's roles |
| /api/rbac/role/<role_name> | GET         | application/json | Return the role's information in JSON |
| /api/rbac/user/<sh_email>  | GET         | application/json | Return the user's permission information in JSON |
| /rbac/roles/update         | POST        | Redirect         | Create/Update/Delete roles |
| /rbac/users/update         | POST        | Redirect         | Modify user's roles |

| **Sessionmgmt URLs**       | HTTP method | Response         | Description                           |
|----------------------------|-------------|------------------|---------------------------------------|
| /session/login             | GET         | text/html        | HTML form for the user to login |
| /session/login/shopper     | POST        | Redirect         | Login |
| /session/logout            | POST/GET    | Redirect         | Logout |

| **Catalogue URLs**         | HTTP method | Response         | Description                           |
|----------------------------|-------------|------------------|---------------------------------------|
| /catalogue                 | GET         | text/html        | View products page (Shopping cart is only visible on desktop due to technical difficulties by Shopping cart group). This is a crappy mock version of catalogue subsystem created by Shopping Cart group for testing purpose. |
| /product/add               | GET         | text/html        | Place holder page for RBAC subsystem testing purpose (because there is little to zero collaboration from them, and they don't share code) |
| /product/edit              | GET         | text/html        | Place holder page for RBAC subsystem testing purpose (because there is little to zero collaboration from them, and they don't share code) |
| /product/delete            | GET         | text/html        | Place holder page for RBAC subsystem testing purpose (because there is little to zero collaboration from them, and they don't share code) |

| **Shopping cart URLs**     | HTTP method | Response         | Description                           |
|----------------------------|-------------|------------------|---------------------------------------|
| /api/cart                  | POST        | application/json | Obtain shopping cart information      |

# API
- /api/rbac/user/Hillary.Clements@sitamet.ca
```json
{
    "permissions": [
        {
            "id": "11",
            "name": "View Users",
            "url": "/rbac/users"
        },
        {
            "id": "12",
            "name": "View Roles",
            "url": "/rbac/roles"
        },
        {
            "id": "13",
            "name": "Obtain user information in JSON format",
            "url": "/api/rbac/user"
        },
        {
            "id": "14",
            "name": "Obtain role information in JSON format",
            "url": "/api/rbac/role"
        }
    ],
    "roles": [
        {
            "id": "20",
            "name": "Admin",
            "url": "A manager who has permission to every commands on the system."
        }
    ],
    "user": "Hillary.Clements@sitamet.ca"
}
```

- /api/rbac/role/Admin
```json
{
    "permissions": [
        {
            "id": "11",
            "name": "View Users",
            "url": "/rbac/users",
            "hasPermission": true
        },
        {
            "id": "12",
            "name": "View Roles",
            "url": "/rbac/roles",
            "hasPermission": true
        },
        {
            "id": "13",
            "name": "Obtain user information in JSON format",
            "url": "/api/rbac/user",
            "hasPermission": true
        },
        {
            "id": "14",
            "name": "Obtain role information in JSON format",
            "url": "/api/rbac/role",
            "hasPermission": true
        }
    ],
    "role_desc": "A manager who has permission to every commands on the system.",
    "role": "Admin"
}
```