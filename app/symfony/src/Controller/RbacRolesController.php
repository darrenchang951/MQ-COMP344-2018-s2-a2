<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Ecom\Rbac\Rbac;
use App\Ecom\Sessionmgmt\Session;

class RbacRolesController extends AbstractController
{
    /**
     * @Route("/rbac/roles", name="rbac_roles")
     * @param Request $request
     * @param Rbac $rbac
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, Rbac $rbac)
    {
        // Check if the client is a logged in shopper
        $sessionmgmt = new Session();
        $shopperId = $sessionmgmt->store_get_shopper_id();
        $shopperName = $sessionmgmt->store_get_shopper_name();

        // check if the client has permission to access the page
        $uri = $request->getRequestUri();
        $uri = parse_url($uri, PHP_URL_PATH);

        if ($rbac->hasPermission($uri)) {
            $roles = $rbac->getRoles();
            $permissions = $rbac->getPermissions();
            return $this->render('rbac_roles/index.html.twig', [
                'title' => 'RBAC Role Management',
                'roles' => $roles,
                'permissions' => $permissions,
                'shopperId' => $shopperId,
                'shopperName' => $shopperName,
            ]);
        } else {
            return $this->render('accessDenied.html.twig', [
                'shopperId' => $shopperId,
                'shopperName' => $shopperName,
            ]);
        }
    }
}
