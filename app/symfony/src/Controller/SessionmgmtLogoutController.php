<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Ecom\Sessionmgmt\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SessionmgmtLogoutController extends AbstractController
{
    /**
     * @Route("/sessionmgmt/logout", name="sessionmgmt_logout", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        // Create object and call logout function
        $session = new Session();
        $session->logout();
        return $this->redirectToRoute('sessionmgmt_login');
    }
}
